/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adivinarnumero;

import java.util.Random;
import java.util.Scanner;

public class AdivinarNumero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random ran = new Random();
        Scanner sc = new Scanner(System.in);
        int aleatorio = ran.nextInt(10) + 1;
        
        int n = 3;
        while (n > 0){
            System.out.println("Adivine un numero del 1 al 10. Num de intentos restantes: " + n);
            int numero = sc.nextInt();
            if (numero == aleatorio){
                System.out.println("Gano!");
                break;
            }
            n--;
        }
        
    }
    
}
